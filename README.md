# sistem-exp

# Antes de Iniciar el Proyecto debemos de tener instalado algunos detalles en el siguiente orden

## 1. cd sistem-exp-api

## 2. composer install

## 3. composer require tymon/jwt-auth:dev-develop --prefer-source

<!-- Primer ejemplo -->

# Antes de probar correr

## php artisan migrate

# Si ya tenes la Base de datos , para borrar todas las tablas usar migrate:reset y luego volver a ejecutar migrate

## php artisan migrate:reset

## php artisan db:seed
