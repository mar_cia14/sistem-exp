<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\uploadfile;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*Esto es para cuando queremos ocupar la ruta> http://127.0.0.1:8000/api/login */
//Route::post('register', 'UserController@register');
//Route::post('login', 'UserController@authenticate');

//Route::post('registro', 'UsuarioController@register');
Route::post('login', 'UsuarioController@authenticate');


    /*RUTAS DE PERMISO*/
    Route::get('permiso', 'Admin\PermisoController@index')->name('permiso');
    Route::get('permiso/crear', 'Admin\PermisoController@crear')->name('crear_permiso');
    Route::post('permiso', 'Admin\PermisoController@guardar')->name('guardar_permiso');
    Route::get('permiso/editar/{id}', 'Admin\PermisoController@editar')->name('editar_permiso');
    Route::put('permiso/{id}', 'Admin\PermisoController@actualizar')->name('actualizar_permiso');
    Route::delete('permiso/{id}', 'Admin\PermisoController@eliminar')->name('eliminar_permiso');
  /*RUTAS DEL MENU*/
    Route::get('menu', 'Admin\MenuController@index')->name('menu');
    Route::get('menu/crear', 'Admin\MenuController@crear')->name('crear_menu');
    Route::get('menu/mostrar/{id}', 'Admin\MenuController@mostrar')->name('mostrar');
    Route::post('menu', 'Admin\MenuController@guardar')->name('guardar_menu');
    Route::get('menu/editar/{id}', 'Admin\MenuController@editar')->name('editar_menu');
    Route::put('menu/{id}', 'Admin\MenuController@actualizar')->name('actualizar_menu');
    Route::get('menu/eliminar/{id}', 'Admin\MenuController@eliminar')->name('eliminar_menu');
    Route::post('menu/guardar-orden', 'Admin\MenuController@guardarOrden')->name('guardar_orden');
    /*RUTAS ROL*/
    Route::get('rol', 'Admin\RolController@index')->name('rol');
    Route::get('rol/crear', 'Admin\RolController@crear')->name('crear_rol');
    Route::post('rol', 'Admin\RolController@guardar')->name('guardar_rol');
    Route::get('rol/{id}/editar', 'Admin\RolController@editar')->name('editar_rol');
    Route::put('rol/{id}', 'Admin\RolController@actualizar')->name('actualizar_rol');
    Route::delete('rol/{id}', 'Admin\RolController@eliminar')->name('eliminar_rol');
    /*RUTAS MENU_ROL*/
    Route::get('menu-rol', 'Admin\MenuRolController@index')->name('menu_rol');
    Route::post('menu-rol', 'Admin\MenuRolController@guardar')->name('guardar_menu_rol');
    /*RUTAS PERMISO_ROL*/
    Route::get('permiso-rol', 'Admin\PermisoRolController@index')->name('permiso_rol');
    Route::post('permiso-rol', 'Admin\PermisoRolController@guardar')->name('guardar_permiso_rol');


        // USUARIOS
    Route::get('usuario', 'UsuarioController@index');
    // Route::get('alumno', 'AlumnoController@index')->name('alumno');
    Route::get('usuario/{usuario}', 'UsuarioController@show');
    Route::get('usuario/usuario/{id}', 'UsuarioController@mostrar')->name('mostrar');
    // Route::post('usuario', 'UsuarioController@store');
    Route::post('usuario', 'UsuarioController@register');//->name('registrar_usuario');
    Route::get('usuario/editar/{id}', 'UsuarioController@editar')->name('editar_usuario');
    Route::put('usuario/{usuario}', 'UsuarioController@update');
    Route::put('usuario/{id}', 'UsuarioController@actualizar')->name('actualizar_usuario');
    Route::delete('usuario/{usuario}', 'UsuarioController@eliminar')->name('eliminar');
    Route::get('usuario/usuario/{id}', 'UsuarioController@eliminar')->name('eliminar_usuario');


 /*RUTAS COLEGIO*/
     Route::get('colegio', 'ColegioController@index');
    Route::get('colegio/{colegio}', 'ColegioController@show');
    Route::post('colegio', 'ColegioController@store');
    Route::put('colegio/{colegio}', 'ColegioController@update');
    Route::delete('colegio/{colegio}', 'ColegioController@delete');
     /*RUTAS DOCENTE*/
    Route::get('docente', 'DocenteController@index');
    Route::get('docente', 'DocenteController@index')->name('docente');
    Route::get('docente/{docente}', 'DocenteController@show');
    Route::get('docente/docente/{id}', 'DocenteController@mostrar')->name('mostrar');
    Route::post('docente', 'DocenteController@store');
    Route::post('docente', 'DocenteController@guardar')->name('guardar_docente');
    Route::get('docente/editar/{id}', 'DocenteController@editar')->name('editar_docente');
    Route::put('docente/{docente}', 'DocenteController@update');
    Route::put('docente/{id}', 'DocenteController@actualizar')->name('actualizar_docente');
    Route::delete('docente/{docente}', 'DocenteController@eliminar');
    Route::get('docente/eliminar/{id}', 'DocenteController@eliminar')->name('eliminar_docente');


// Documentos
// Route::apiResource('documento','Documento\Documento');
// Route::get('file\documento_list','DocumentoController\documentoList');
// Route::post('process', function (Request $request) {
//     $path = $request->file('photo')->store('photos');

//     dd($path);
// });

Route::delete('/file/{id}', 'Auth\DocumentController@delete');
Route::get('/file/{id}', 'Auth\DocumentController@ver');
Route::get('/file','Auth\DocumentController@download');
// Route::get('/file/download/{id}', 'Auth\DocumentController@show')->name('downloadfile');


Route::get('documento', 'Auth\DocumentController@index');
Route::post('documento', 'Auth\DocumentController@store');
Route::get('/documento/download/{id}', 'Auth\DocumentController@show');
Route::get('/documento/{id}','Auth\DocumentController@mostrar');
Route::delete('/documento/{id}', 'Auth\DocumentController@eliminar');


//  Route::post('storage/create', 'StorageController@save');
// Route::get('storage/{archivo}', function ($archivo) {
//      $public_path = public_path();
//      $url = $public_path.'/storage/'.$archivo;
//      //verificamos si el archivo existe y lo retornamos
//      if (Storage::exists($archivo))
//      {
//        return response()->download($url);
//      }
//      //si no se encuentra lanzamos un error 404.
//      abort(404);

// });

// Route::post('media', function () {
//     request()->validate(['file' => 'image']);
//     return request()->file->storeAs('uploads', request()->file->getClientOriginalName());
// });

// Route::get('/uploads/{file}', function ($file) {
//     return Storage::response("uploads/$file");
// });

//  Route::get('/uploads/{file}', function ($file) {
//     return Storage::response("uploads/$file");
// })->where([
//     'file' => '(.*?)\.(jpg|png|jpeg|gif)$'
// ]);

 /*RUTAS ALUMNOS*/

//  Route::get('/', function () {
//     return 'alumno'; 'AlumnoController@index';
// });

// Route::middleware('auth:api')->get('/alumno', function (Request $request) {
//     return $request->alumno();
// });

// Route::get('/', function () {
//     return 'alumno'; 'AlumnoController@index';
// });

// Route::get('/', function () {
//     return ['alumno', 'AlumnoController@index'];
// });



     Route::get('alumno', 'AlumnoController@index');
    // Route::get('alumno', 'AlumnoController@index')->name('alumno');
    Route::get('alumno/{alumno}', 'AlumnoController@show');
    Route::get('alumno/alumno/{id}', 'AlumnoController@mostrar')->name('mostrar');
    // Route::post('alumno', 'AlumnoController@store');

    Route::post('alumno', 'AlumnoController@guardar')->name('guardar_alumno');
    Route::put('alumno/{id}', 'AlumnoController@editar');//->name('editar_alumno');
    //Route::put('alumno/{alumno}', 'AlumnoController@update');
    //Route::put('alumno/{alumno}', 'AlumnoController@update');
   //Route::put('alumno/{alumno}', 'AlumnoController@update');
    Route::delete('alumno/{alumno}', 'AlumnoController@eliminar');
    // Route::get('alumno/eliminar/{id}', 'AlumnoController@eliminar')->name('eliminar_alumno');

Route::group(['middleware' => ['jwt.verify']], function()
{
           /*AÑADE AQUI LAS RUTAS QUE QUIERAS PROTEGER CON JWT*/
           Route::get('/tareas', 'TaskController@index');
           Route::put('/tareas/actualizar', 'TaskController@update');
           Route::post('/tareas/guardar', 'TaskController@store');
           Route::delete('/tareas/borrar/{id}', 'TaskController@destroy');
           Route::get('/tareas/buscar', 'TaskController@show');

 });


/*Esto es para cuando queremos ocupar la ruta>http://127.0.0.1:8000/api/auth/login */
// Route::group([
//     'middleware' => 'api',
//     'prefix' => 'auth'

// ], function ($router) {
//     Route::post('login', 'AuthController@login');
//     Route::post('logout', 'AuthController@logout');
//     Route::post('refresh', 'AuthController@refresh');
//     Route::post('me', 'AuthController@me');
// });


// Route::group([

//     'middleware' => 'api',
//     'namespace' => 'App\Http\Controllers',
//     'prefix' => 'auth'

// ], function ($router) {

//     Route::post('login', 'AuthController@login');
//     Route::post('logout', 'AuthController@logout');
//     Route::post('refresh', 'AuthController@refresh');
//     Route::post('me', 'AuthController@me');

// });
