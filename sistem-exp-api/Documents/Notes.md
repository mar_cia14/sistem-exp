<!-- Primer ejemplo -->

# Antes de probar correr

## php artisan migrate

## php artisan db:seed

## Primer ejemplo

# install

composer require spatie/laravel-permission
composer require intervention/image

<!-- En Routers\api.php -->

<!-- \_Esto es para cuando queremos ocupar la ruta> http://127.0.0.1:8000/api/login -->

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@authenticate');

Route::post('registro', 'UsuarioController@register');
Route::post('login-es', 'UsuarioController@authenticate');

Route::group(['middleware' => ['jwt.verify']], function() {
_AÑADE AQUI LAS RUTAS QUE QUIERAS PROTEGER CON JWT_/
});

<!-- Correr -->

php artisan serve

<!-- En Insomnia usamos la ruta http://127.0.0.1:8000/api/login -->

http://127.0.0.1:8000/api/login?email=admin%40admin.com&password=pass123

email: admin@admin.com
password : pass123
![Ejemplo # 1](..\Documents\1.png)

## Segundo ejemplo

<!-- En Routers\api.php -->

<!-- /_Esto es para cuando queremos ocupar la ruta>http://127.0.0.1:8000/api/auth/login _/ -->

Route::group([
'middleware' => 'api',
'prefix' => 'auth'

], function (\$router) {
Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');
Route::post('refresh', 'AuthController@refresh');
Route::post('me', 'AuthController@me');
});

<!-- Correr -->

php artisan serve

http://127.0.0.1:8000/api/auth/login?email=admin%40admin.com&password=pass123

email: admin@admin.com
password : pass123

![Ejemplo # 2](..\Documents\2.png)

Link de Interes
https://github.com/gothinkster/laravel-realworld-example-app

https://desarrollowebtutorial.com/generar-pdf-en-laravel/

Spatie\Permission\PermissionServiceProvider
