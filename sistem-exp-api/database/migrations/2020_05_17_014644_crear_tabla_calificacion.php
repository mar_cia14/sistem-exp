<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaCalificacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calificacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha')->nullable();
            $table->string('nombre', 50);
            $table->unsignedTinyInteger('nota1')->default(0);
            $table->unsignedBigInteger('alumno_id');
            $table->foreign('alumno_id','fk_calificacion_alumno')->references('id')->on('alumno')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger('docente_id');
            $table->timestamps();
            $table->charset ='utf8mb4';
            $table->collation ='utf8mb4_spanish_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calificacion');
    }
}
