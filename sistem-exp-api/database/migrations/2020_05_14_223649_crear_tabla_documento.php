<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documento', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            // $table->string('consecutivo', 20);
            $table->date('fecha')->nullable();
            $table->string('slug', 255);
            $table->string('tipo_documento', 255)->nullable();
             $table->unsignedBigInteger('alumno_id');
            // $table->unsignedBigInteger('alumno_id')->default(1);
            // $table->foreign('alumno_id', 'fk_alumno_documento')->references('id')->on('alumno')->onDelete('cascade')->onUpdate('cascade');
            // $table->unsignedBigInteger('docente_id');
            $table->timestamps();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_spanish_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documento');
    }
}
