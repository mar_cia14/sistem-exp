<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaDocente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 50);
            $table->string('apellido', 50);
            $table->string('direccion', 255);
            $table->string('telefono1', 20)->unique();
            $table->string('telefono2', 20)->nullable();
            $table->string('observaciones', 255)->nullable();
            $table->string('email', 100)->nullable();
            // $table->string('matricula', 50)->unique();
            $table->string('foto', 100)->nullable();
           $table->unsignedBigInteger('colegio_id');
            $table->foreign('colegio_id', 'fk_colegio_docente')->references('id')->on('colegio')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id', 'fk_dcalumno_usuario')->references('id')->on('usuario')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_spanish_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docente');
    }
}
