<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaAlumno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumno', function (Blueprint $table) {
          $table->bigIncrements('id');
            $table->string('nombre', 50);
            $table->string('apellido_paterno', 50)->nullable();
            $table->string('apellido_materno', 50)->nullable();
            $table->string('sexo',1)->nullable();
            $table->string('direccion', 255)->nullable();
            $table->string('telefono1', 20)->unique()->nullable();
            $table->string('telefono2', 20)->nullable();
            $table->string('email', 100)->unique()->nullable();
            $table->string('observaciones', 255)->nullable();
            $table->string('tutor', 50)->nullable()->nullable();
            $table->string('telefono_tutor', 20)->nullable();
            $table->string('email_tutor', 100)->nullable();
            $table->date('fecha_inicio_escolaridad')->nullable();
            $table->date('fecha_termino_escolaridad')->nullable();
            $table->string('foto', 100)->nullable();
            $table->string('nombre_completo', 100)->nullable();
            $table->unsignedBigInteger('colegio_id')->default(1);
            $table->foreign('colegio_id', 'fk_colegio_alumno')->references('id')->on('colegio')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger('usuario_id')->default(1);
            $table->foreign('usuario_id', 'fk_alumno_usuario')->references('id')->on('usuario')->onDelete('restrict')->onUpdate('restrict');
            $table->boolean('activo')->nullable();
            $table->timestamps();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_spanish_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumno');
    }
}
