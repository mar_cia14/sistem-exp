<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
/* use App\Models\Seguridad\Usuario; */
use App\Models\Usuario;
use Illuminate\Support\Facades\Hash;

class TablaUsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $usuario = Usuario::create([
            'usuario' => 'admin',
            'nombre' => 'Administrador',
            'email' => 'admin@admin.com',
            'password' => Hash::make('pass123')
        ]);

        $usuario->roles()->sync(1);
    }
}
