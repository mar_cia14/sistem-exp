<?php

use Illuminate\Database\Seeder;
//use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTablas([
            'rol',
            'menu',
            'permiso',
            'menu_rol',
            'permiso_rol',
            'usuario',
            'usuario_rol',
            'colegio'
        ]);
        $this->call(TablaRolSeeder::class);
        $this->call(TablaMenuSeeder::class);
        $this->call(TablaMenuRolSeeder::class);
        $this->call(TablaPermisoSeeder::class);
        $this->call(TablaUsuarioSeeder::class);
        $this->call(TablaUserSeeder::class);
        $this->call(TablaColegioSeeder::class);
    }

    protected function truncateTablas(array $tablas)
    {
      //$tablas::truncate();
    //  DB::table($tabla)->truncate();
       /*  DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        foreach ($tablas as $tabla) {
            DB::table($tabla)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); */
    }
}
