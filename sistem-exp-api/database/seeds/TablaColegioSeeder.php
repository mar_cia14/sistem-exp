<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TablaColegioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         $now = Carbon::now()->toDateTimeString();
        $colegio = [
            array(
            'nombre' => 'Colegio Nuevo México',
            'descripcion' => 'Colegio Autonomo Nuevo Mexico',
            'direccion' => 'Nuevo Mexico',
            'telefono1' => '2559-0088',
            'email' => 'colegio.nuevomexico@colegio.com',
            'fecha_fundacion' => '12/05/2000',
           'created_at' => $now,
           'updated_at' => $now
        ),
        ];
        DB::table('colegio')->insert($colegio);

    }
}
