<!DOCTYPE html>
<head>
  <title>Laravel</title>
</head>
<body>
  <table>
    <tr>
      <th>SL</th>
      <th>Titulo</th>
      <th>TipoDocumento</th>
      <th>Ver</th>
      <th>Download</th>
    </tr>
    @foreach($file as $key => $data)
    <tr>
      <th>{{++$key}}</th>
      <th>{{$data -> titulo}}</th>
      <th>{{$data -> tipo_documento}}</th>
      <th><a href="/api/files/{{$data -> id}}"> Ver</th>
      <th><a href="/api/files/download/{{$data -> id}}"> Download</th>
    </tr>
    @endforeach
  </table>

</body>
</html>
