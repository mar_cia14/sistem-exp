<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Rol extends Model
{
      protected $table = "rol";
     // protected $primaryKey = "id";
      protected $fillable=["nombre"];
    //  protected $guarded=['id'];
}
