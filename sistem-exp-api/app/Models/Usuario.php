<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
//use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Foundation\Auth\Usuario as Authenticatable;
use App\Models\Rol;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
//Añadimos la clase JWTSubject
use Tymon\JWTAuth\Contracts\JWTSubject;

//Cambiar cuando se cree la auntencticacion
//class Usuario extends Authenticatable
//Añadimos la implementación de JWT en nuestro modelo
class Usuario extends Authenticatable implements JWTSubject
//class Usuario extends  Model
{
    use Notifiable;
    protected $remember_token = false;
    protected $table = 'usuario';
    protected $fillable = ['usuario', 'nombre', 'email', 'password'];

    protected $hidden = [
        'password', 'remember_token',
    ];

        /*
            Añadiremos estos dos métodos
        */
    public function getJWTIdentifier()
    {
            return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
            return [];
     }


    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;

    }

    public function roles()
    {
        return $this->belongsToMany(Rol::class, 'usuario_rol');
    }

    public function setSession($roles)
    {
        Session::put([
            'usuario' => $this->usuario,
            'usuario_id' => $this->id,
            'nombre_usuario' => $this->nombre
        ]);
        if (count($roles) == 1) {
            Session::put(
                [
                    'rol_id' => $roles[0]['id'],
                    'rol_nombre' => $roles[0]['nombre'],
                ]
            );
        } else {
            Session::put('roles', $roles);
        }
    }

    /* public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = Hash::make($pass);
    } */

}
