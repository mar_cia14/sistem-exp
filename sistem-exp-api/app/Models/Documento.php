<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;


class Documento extends Model
{
    protected $table = "documento";
    protected $fillable = ['titulo','file', 'fecha','slug', 'tipo_documento','alumno_id'];
    // protected $fillable = ['path'];

     public function documento()
    {
        return $this->HasMany(Documento::class);
    }


    //Query Scope

// public function scopeSearch($query, $find)
// {
//     $search = explode(" ", $find);
//     $total = count($search);

//     $array = array();
//     for($i=0; $i<$total; $i++ ) {
//          if( $i == 0 ) {
//               $array = $query->where(Documento::raw("CONCAT(alumno.nombre, ' ', apellido_paterno, ' ', apellido_materno, ' ')"), "LIKE", "%".($search[$i])."%")
//               ->pluck('nombre', 'id')->toArray();
//             //   ->get();
//           } else {
//               $array = $array->orWhere(Documento::raw("CONCAT(alumno.nombre, ' ', apellido_paterno, ' ', apellido_materno, ' ')"), "LIKE", "%".($search[$i])."%")
//               ->pluck('nombre', 'id')->toArray();
//             //   ->get();
//           }
//     }
//     return $array;
// }

        public function scopeAlumnoId($query, $id)
    {
        if($id)
            return $query->where('alumno_id', 'LIKE', "%$id%");
    }

    public function scopeName($query, $nombre)
    {
        if($nombre)
            return $query->where('nombre', 'LIKE', "%$name%");
    }

    public function scopeEmail($query, $email)
    {
        if($email)
            return $query->where('email', 'LIKE', "%$email%");
    }

        public function scopeApellido($query, $apellido)
    {
        if($apellido)
            return $query->where('apellido', 'LIKE', "%$apellido%");
    }




    public function getUrlPathAttibute(){
        // return \Storage:: url($this->slug);
        return Storage::url($this->slug, ‘storage’);
        //return \Storage:: url($this->path);
        // https://www.laraveltip.com/como-mostrar-imagenes-de-la-carpeta-storage-en-laravel/
    }

    public static function setFoto($foto, $actual = false)
    {
        if ($foto) {
            if ($actual) {
                Storage::disk('public')->delete("imagenes/caratulas/$actual");
            }
            $imageName = Str::random(20) . '.jpg';
            $imagen = Image::make($foto)->encode('jpg', 75);
            $imagen->resize(530, 470, function ($constraint) {
                $constraint->upsize();
            });
            Storage::disk('public')->put("imagenes/caratulas/$imageName", $imagen->stream());
            return $imageName;
        } else {
            return false;
        }
    }

}
