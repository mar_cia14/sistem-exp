<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;


class Alumno extends Model
{
     protected $table = "alumno";
    protected $fillable = ['nombre', 'apellido_paterno','apellido_materno', 'direccion', 'telefono1', 'telefono2', 'observaciones', 'tutor','telefono_tutor','email','email_tutor','sexo','fecha_inicio_escolaridad','fecha_termino_escolaridad','foto','nombre_completo',
    'colegio_id','usuario_id','activo'];

     public function alumno()
    {
        return $this->HasMany(Alumno::class);
    }

    public static function setFoto($foto, $actual = false)
    {
        if ($foto) {
            if ($actual) {
                Storage::disk('public')->delete("imagenes/caratulas/$actual");
            }
            $imageName = Str::random(20) . '.jpg';
            $imagen = Image::make($foto)->encode('jpg', 75);
            $imagen->resize(530, 470, function ($constraint) {
                $constraint->upsize();
            });
            Storage::disk('public')->put("imagenes/caratulas/$imageName", $imagen->stream());
            return $imageName;
        } else {
            return false;
        }
    }

}
