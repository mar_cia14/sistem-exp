<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Permiso;
use App\Http\Requests\ValidarPermiso;

class PermisoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // // De esta forma dilata 2.28 Segundos
         $permisos = Permiso::get();
        return compact('permisos');

          // // De esta forma dilata 9.02 Segundos
           //      $permisos = Permiso::orderBy('id')->get();
           //     // return view('admin.permiso.index', compact('permisos'));
          //     return Permiso::all();
          // // return $permisos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        // return view('admin.permiso.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(ValidarPermiso $request)
    {
        $table = Permiso::create($request->all());
        return response()->json($table);

        // Permiso::create($request->all());
        // return redirect('admin/permiso/crear')->with('mensaje', 'Permiso creado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mostrar($id)
    {
         $table = Permiso::findOrFail($request->id);
        return $table;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Permiso::findOrFail($id);
        return  compact('data');
        // return view('admin.permiso.editar', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function actualizar(ValidarPermiso $request, Alumno $table, $id)
    public function actualizar(Request $request,  $id)
    {
         $table = Permiso::findOrFail($id);
        $table->update(array_filter($request->all()));
        return response()->json($table, 200);
        //  Permiso::findOrFail($id)->update($request->all());
        // // return redirect('admin/permiso')->with('mensaje', 'Permiso actualizado con exito');
        // return response()->json($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Permiso::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
