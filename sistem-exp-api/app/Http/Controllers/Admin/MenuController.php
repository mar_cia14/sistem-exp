<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Menu;
use App\Http\Requests\ValidacionMenu;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $menus = Menu::all();
        // return $menus;
        // // Esta función nos devolvera todas las tareas que tenemos en nuestra BD
         // De esta forma dilata 7.28 Segundos
        $menus = Menu::getMenu();
        return compact('menus');

    }

    // public function mostrar($id)
    public function mostrar(Request $request)
    {
        // return $table;
        // return Menu::where('id', $id)->get();
         $table = Menu::findOrFail($request->id);
        return $table;
         //Esta función devolverá los datos de una tarea que hayamos seleccionado para cargar el formulario con sus datos
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crear()
    {
        // return view('admin.menu.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function guardar(ValidacionMenu $request)
    public function guardar(Request $request)
    {
        $table = Menu::create($request->all());
        return response()->json($table);

        //  //Instanciamos la clase Menu
        // $table = new Menu;
        // //Declaramos el nombre con el nombre enviado en el request
        // $table->nombre = $request->nombre;
        // $table->menu_id = $request->menu_id;
        // $table->url = $request->url;
        // $table->orden = $request->orden;
        // $table->icono = $request->icono;
        // //Guardamos el cambio en nuestro modelo
        // $table->save();

    //   return response()->json($table);
    //     // Menu::create($request->all());
    //     // return redirect('admin/menu/crear')->with('mensaje', 'Menú creado con exito');

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Menu::findOrFail($id);
        // return view('admin.menu.editar', compact('data'));
        return  compact('data');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function actualizar(ValidacionMenu $request, $id)
    //  public function actualizar(Request $request, Menu $table)
    public function actualizar(Request $request,  $id)
    {
        $table = Menu::findOrFail($id);
        $table->update(array_filter($request->all()));
        return response()->json($table, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
   {
        Menu::destroy($id);
         return response()->json(null, 204);
        // return response()->json(['mensaje' => 'Menú eliminado con exito']);
    }

    public function guardarOrden(Request $request)
    {
        if ($request->ajax()) {
            $menu = new Menu;
            $menu->guardarOrden($request->menu);
            return response()->json(['respuesta' => 'ok']);
        } else {
            abort(404);
        }
    }
}
