<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use App\Documento;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\Alumno;
use Illuminate\Database\Eloquent\Collection;

class DocumentController extends Controller
{

    public function index(Request $request)
  {
       $Listado_documentos = Documento::select('documento.id', 'documento.titulo','documento.fecha','documento.slug','documento.tipo_documento','documento.alumno_id',
       'alumno.nombre', 'alumno.apellido_paterno','alumno.apellido_materno', 'alumno.nombre_completo'
       )
     ->join('alumno', 'alumno.id', '=', 'documento.alumno_id')
     ->where("alumno.nombre_completo", "like", $request->texto."%")->take(10)
      ->orWhere('alumno.apellido_paterno','like', $request->texto."%")
      ->orWhere('alumno.apellido_materno','like', $request->texto."%")
     ->get();

      return $Listado_documentos;


    // $Listado_documentos = Documento::join('alumno', 'alumno.id', '=', 'documento.alumno_id')
    //                 // ->selectRaw('documento.id', 'documento.titulo','documento.fecha','documento.slug','documento.tipo_documento','documento.alumno_id', 'alumno.nombre', 'alumno.apellido_paterno','alumno.apellido_materno')
    //                 ->selectRaw('documento.id, documento.titulo,documento.fecha,documento.slug,documento.tipo_documento,documento.alumno_id, alumno.nombre, alumno.apellido_paterno,alumno.apellido_materno')
    //                 ->groupBy('documento.id', 'documento.titulo','documento.fecha','documento.slug','documento.tipo_documento','documento.alumno_id', 'alumno.nombre', 'alumno.apellido_paterno','alumno.apellido_materno')
    //                 ->search($request->Listado_documentos);
                    // ->paginate(12);
  }

  public function store(Request $request)
  {
        $validator = Validator::make($request->all(),
              [
           //   'user_id' => 'required',
               'tipodocumento'=>'required',
               'alumnoid'=> 'required',
               'file' => 'required|mimes:doc,docx,pdf,txt,png,jpg,jpeg,md|max:2048',
             ]);

       if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
         }

        //  ///store file into document folder
        //    $file = $request->file->store('public/documents/'.$request->alumno_id . '/',$request->tipodocumento .'_Alumno_' .$request->alumno_id.'.'.$extension);
        //   Storage::disk('public')->put("public/documents/.$request->alumno_id . '/',$request->tipodocumento .'_Alumno_' .$request->alumno_id.'.'.$extension", $imagen->stream());

        /*Esto ya Funciona */
        if ($files = $request->file('file')) {

           $path = $request->file->path();
           $extension = $request->file->extension();
           $file = $request->file->storeAs('public/storage/documents/'.$request->alumnoid . '/',$request->tipodocumento .'_Alumno_' .$request->alumnoid.'.'.$extension);

        // // // $file = $request->file->store('public/documents/'.$request->alumno_id . '/',$request->tipodocumento .'_Alumno_' .$request->alumno_id.'.'.$extension);
        // // $file = $request->file->storeAs( public_path('/documents/'.$request->alumno_id . '/',$request->tipodocumento .'_Alumno_' .$request->alumno_id.'.'.$extension));

            $document = new Documento();
            $document->titulo = $request->tipodocumento.'_Alumno_' .$request->alumnoid.'.'.$extension;
            $document->slug = 'public/storage/documents/'.$request->alumnoid . '/'.$request->tipodocumento .'_Alumno_' .$request->alumnoid.'.'.$extension;
            // $document->slug = $request->tipodocumento .'_Alumno_' .$request->alumno_id.'.'.$extension;
        //   //$document->slug = $file.$request->tipodocumento . ' ' .'Alumno_' .$request->alumno_id;
            $document->alumno_id = $request->alumnoid ;
            $document->fecha = Carbon::now();
            $document->tipo_documento = $request->tipodocumento ;
            $document->save();

          return response()->json(['Success'=> true], 200);
          }
  }

   public function mostrar(Request $request)
    {
         return Documento::all();
    }

    public function eliminar($id)
    {
          $data = Documento::find($id);
         Storage::delete($data->titulo);

         Documento::destroy($id);
         return response()->json(null, 204);
    }

    public function ver($id)
  {
      $data = Documento :: find($id);
      return view('details', compact('data'));
  }

   public function download(){
        //$downloads=DB::table('Documento')->get();
         $downloads=Documento::all();
        return view('download.viewfile',compact('downloads'));
    }


    public function show($id)
    {
        $data = Documento::find($id);
        // return  storage::download($data->slug, $data->titulo);
        return Storage::download($data->slug, $data->titulo);
    }

  /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(Request $request)
    {
        $file = $request->file('file');

        $path = Storage::disk('local')->path("chunks/{$file->getClientOriginalName()}");

        File::append($path, $file->get());

        if ($request->has('is_last') && $request->boolean('is_last')) {
            $name = basename($path, '.part');

            File::move($path, "/path/to/public/someid/{$name}");
        }


        return response()->json(['uploaded' => true]);
    }


    public function save()
  {
       request()->validate([
         'file'  => 'required|mimes:doc,docx,pdf,txt|max:2048',
       ]);

       if ($files = $request->file('fileUpload')) {
           $destinationPath = 'public/file/'; // upload path
           $profilefile = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $profilefile);
           $insert['titulo'] = "$profilefile";
        }

        $check = Documento::insertGetId($insert);

        return Redirect::to("file")
        ->withSuccess('Great! file has been successfully uploaded.');

  }

}
