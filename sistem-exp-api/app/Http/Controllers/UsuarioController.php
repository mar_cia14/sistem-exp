<?php
namespace App\Http\Controllers;

    use App\Models\Usuario;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Validator;
    use JWTAuth;
    use Tymon\JWTAuth\Exceptions\JWTException;

class UsuarioController extends Controller
{
    public function index()
    {
       return Usuario::all();
    }

 public function show(Usuario $table)
    {
        return $table;
    }

    public function mostrar(Request $request)
    {
         $table = Usuario::findOrFail($request->id);
        return $table;
         //Esta función devolverá los datos de una tarea que hayamos seleccionado para cargar el formulario con sus datos
    }

     public function store(Request $request)
    {
        $table = Usuario::create($request->all());

        return response()->json($table);
    }

        public function guardar(Request $request)
    {
        $table = Usuario::create($request->all());

        return response()->json($table);
    }

     public function update(Request $request, Usuario $table)
    {
        $table->update($request->all());

        return response()->json($table);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Usuario::findOrFail($id);
        // return view('admin.menu.editar', compact('data'));
        return  compact('data');
    }

    // public function delete(Alumno $table)
   public function eliminar($id)
    {
       // Usuario_rol::destroy($id);
        Usuario::destroy($id);
         return response()->json(null, 204);

        // $usuario = Usuario::find($id);
        // $usuario->roles()>delete();
        // $usuario->delete();
        //  return response()->json(null, 204);
    }


    //    public function eliminar(Request $request, $id)
    //     {
    //     if ($request->ajax()) {
    //         $usuario = Usuario::findOrFail($id);
    //         $usuario->roles()->detach();
    //         $usuario->delete();
    //         return response()->json(['mensaje' => 'ok']);
    //      } else {
    //         abort(404);
    //     }
    // }




    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['usuario_not_found'], 404);
            }
            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                    return response()->json(['token_expired'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                    return response()->json(['token_invalid'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['token_absent'], $e->getStatusCode());
            }
            return response()->json(compact('usuario'));
    }

    public function register(Request $request)
        {
                $validator = Validator::make($request->all(), [
                'nombre' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);

            if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
            }

            $user = Usuario::create([
                'usuario' => $request->get('usuario'),
                'nombre' => $request->get('nombre'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                //  'api_token' => JWTAuth::fromUser($user),
                // 'rememberToken' => JWTAuth::fromUser($user),
            ]);

             $token = JWTAuth::fromUser($user);

            $user->roles()->sync(1);

            //return response()->json(compact($user,'token'),200);
            return response()->json($user);
            //  return response()->json(compact('usuario','token'),201);

        }

}
