<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
Use App\Documento;

class DocumentoController extends Controller
{

    // public function index()
    // {
    //    //  return Documento::all();
    //    // return view('documento');
    // }

    public function documentoList()
    {
        // return response()->download(public_path('successkid.jpg'), 'User Image');
    return response()->download(public_path('successkid.jpg'), 'User Image');
        //   return response()->download(public_path('Fondo1.jpeg'), $name, $headers);
    }


    public function index() {
      return view('uploadfile');
   }

   public function fileDownload(){
        //Suppose profile.docx file is stored under project/public/download/profile.docx
        $file= public_path(). "/download/profile.docx";
        $headers = array(
              'Content-Type: application/octet-stream',
            );
        return Response::download($file, 'my-filename.docx', $headers);
}

   public function showUploadFile(Request $request) {
      $file = $request->file('image');

      //Display File Name
      echo 'File Name: '.$file->getClientOriginalName();
      echo '<br>';

      //Display File Extension
      echo 'File Extension: '.$file->getClientOriginalExtension();
      echo '<br>';

      //Display File Real Path
      echo 'File Real Path: '.$file->getRealPath();
      echo '<br>';

      //Display File Size
      echo 'File Size: '.$file->getSize();
      echo '<br>';

      //Display File Mime Type
      echo 'File Mime Type: '.$file->getMimeType();

      //Move Uploaded File
      $destinationPath = 'uploads';
      $file->move($destinationPath,$file->getClientOriginalName());
   }




    public function guardar()
    {
       request()->validate([
         'documento'  => 'required|mimes:doc,docx,pdf,txt|max:2048',
       ]);

       if ($files = $request->file('fileUpload')) {
           $destinationPath = 'public/file/'; // upload path
           $profilefile = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $profilefile);
           $insert['titulo'] = "$profilefile";
        }

        $check = Document::insertGetId($insert);

        return Redirect::to("documento")
        ->withSuccess('Great! file has been successfully uploaded.');

    }



//     public function save(Request $request)
//     {
//        //obtenemos el campo file definido en el formulario
//        $file = $request->file('file');

//        //obtenemos el nombre del archivo
//        $nombre = $file->getClientOriginalName();

//        //indicamos que queremos guardar un nuevo archivo en el disco local
//        \Storage::disk('local')->put($nombre,  \File::get($file));

//        return "archivo guardado";
//    }

//    public function save()
//    {
//         request()->validate([
//         'file'  => 'required|mimes:doc,docx,pdf,txt|max:2048',]);
//             if ($files = $request->file('fileUpload')) {
//              $destinationPath = 'public/file/'; // upload path
//                 $profilefile = date('YmdHis') . "." . $files->getClientOriginalExtension();
//              $files->move($destinationPath, $profilefile);
//              $insert['file'] = "$profilefile";
//             }

//         $check = Documento::insertGetId($insert);
//         return Redirect::to("file")->withSuccess('Great! file has been successfully uploaded.');
//     }

}
