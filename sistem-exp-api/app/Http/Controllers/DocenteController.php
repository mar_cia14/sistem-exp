<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Docente;

class DocenteController extends Controller
{
    public function index()
    {
        return Docente::all();
    }

    public function show(Docente $table)
    {
        return $table;
    }

    public function mostrar(Request $request)
    {
        // return $table;
        // return Menu::where('id', $id)->get();
         $table = Docente::findOrFail($request->id);
        return $table;
         //Esta función devolverá los datos de una tarea que hayamos seleccionado para cargar el formulario con sus datos
    }


    public function store(Request $request)
    {
        $table = Docente::create($request->all());

        return response()->json($table);
    }

    public function update(Request $request, Docente $table)
    {
        $table->update($request->all());

        return response()->json($table);
    }

       /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $data = Docente::findOrFail($id);
        // return view('admin.menu.editar', compact('data'));
        return  compact('data');
    }

    // public function delete(Alumno $table)
   public function eliminar($id)
    {
        Alumno::destroy($id);
         return response()->json(null, 204);
        // $table->delete();
        // return response()->json(null, 204);
    }
}
