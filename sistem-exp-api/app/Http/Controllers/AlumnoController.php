<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Alumno;
use Carbon\Carbon;
class AlumnoController extends Controller
{
    public function index()
    {
       return Alumno::all();
    }

    public function show(Alumno $table)
    {
        return $table;
    }

    public function mostrar(Request $request)
    {
        // return $table;
        // return Menu::where('id', $id)->get();
         $table = Alumno::findOrFail($request->id);
        return $table;
         //Esta función devolverá los datos de una tarea que hayamos seleccionado para cargar el formulario con sus datos
    }

    public function store(Request $request)
    {
        //$table = Alumno::create($request->all());
        // return response()->json($table);

           $alumno = new Alumno();
           $alumno->nombre =$request->nombre;
           $alumno->apellido_paterno =$request->apellido;
           $alumno->apellido_materno =$request->apellido_materno;
           $alumno->sexo =$request->sexo;
           $alumno->direccion =$request->direccion ;
           $alumno->telefono1 =$request->telefono1;
           $alumno->telefono2 =$request->telefono2 ;
           $alumno->email =$request->email ;
           $alumno->observaciones =$request->observaciones;
           $alumno->tutor =$request->tutor;
           $alumno->telefono_tutor =$request->telefono_tutor;
           $alumno->email_tutor =$request->email_tutor;
           $alumno->fecha_inicio_escolaridad =$request->fecha_inicio_escolaridad;
           $alumno->fecha_termino_escolaridad =$request->fecha_termino_escolaridad;
           $alumno->nombre_completo =$request->nombre.' '.$request->apellido_paterno.' '.$request->apellido_materno;
           $alumno->colegio_id =1;//$request->colegio_id;
           $alumno->activo =true;//$request->activo;
           $alumno->fecha = Carbon::now();
           $alumno->save();




    }

     public function guardar(Request $request)
    {
        // $table = Alumno::create($request->all());
        // return response()->json($table);
          $alumno = new Alumno();
           $alumno->nombre =$request->nombre;
           $alumno->apellido_paterno =$request->apellido_paterno;
           $alumno->apellido_materno =$request->apellido_materno;
           $alumno->sexo =$request->sexo;
           $alumno->direccion =$request->direccion ;
           $alumno->telefono1 =$request->telefono1;
           $alumno->telefono2 =$request->telefono2 ;
           $alumno->email =$request->email ;
           $alumno->observaciones =$request->observaciones;
           $alumno->tutor =$request->tutor;
           $alumno->telefono_tutor =$request->telefono_tutor;
           $alumno->email_tutor =$request->email_tutor;
           $alumno->fecha_inicio_escolaridad =$request->fecha_inicio_escolaridad;
           $alumno->fecha_termino_escolaridad =$request->fecha_termino_escolaridad;
           $alumno->nombre_completo =$request->nombre.' '.$request->apellido_paterno.' '.$request->apellido_materno;
           $alumno->colegio_id =1; //$request->colegio_id;
           $alumno->activo =true;//$request->activo;
        //    $alumno->create_at = Carbon::now();
           $alumno->save();

            //  return response()->json(['Success'=> true], 200);
              return response()->json($alumno);
    }

    public function update(Request $request, Alumno $table)
    {
        $table->update($request->all());

        return response()->json($table, 200);
    }

       /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar(Request $request, $id)
    {
          $table = Alumno::findOrFail($id);
        $table->update($request->all());

        return $table;
        //  return response()->json($table);

        // $data = Alumno::findOrFail($id);
        // // return view('admin.menu.editar', compact('data'));
        // return  compact('data');
    }

    public function actualizar($id)
    {
        $table = Alumno::findOrFail($id);
        $table->update(array_filter($request->all()));

         return response()->json($table);
        //  return  compact('data');
        // return redirect('admin/usuario')->with('mensaje', 'Usuario actualizado con exito');
    }

    // public function delete(Alumno $table)
   public function eliminar($id)
    {
        Alumno::destroy($id);
         return response()->json(null, 204);
        // $table->delete();
        // return response()->json(null, 204);
    }



}
