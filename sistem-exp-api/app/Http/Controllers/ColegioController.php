<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Colegio;

class ColegioController extends Controller
{
    public function index()
    {
        return Colegio::all();
    }

    public function show(Colegio $table)
    {
        return $table;
    }

    public function store(Request $request)
    {
        $table = Colegio::create($request->all());

        return response()->json($table);
    }

    public function update(Request $request, Colegio $table)
    {
        $table->update($request->all());

        return response()->json($table);
    }

    public function delete(Colegio $table)
    {
        $table->delete();

        return response()->json(null, 204);
    }
}
